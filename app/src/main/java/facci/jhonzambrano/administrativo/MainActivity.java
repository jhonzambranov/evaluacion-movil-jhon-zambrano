package facci.jhonzambrano.administrativo;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.jhonzambrano.administrativo.unidades.utilidades;

public class MainActivity extends AppCompatActivity {
    EditText  campoCedula, campoNombre, campoApellido, campoDepartamento, campoFecha;
    Button buttonConsulta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonConsulta = findViewById(R.id.btnConsultar);
        buttonConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Mostrar.class);
                startActivity(intent);
            }
        });

        campoCedula = (EditText) findViewById(R.id.etCedula);
        campoNombre = (EditText) findViewById(R.id.etNombres);
        campoApellido = (EditText) findViewById(R.id.etApellidos);
        campoDepartamento = (EditText) findViewById(R.id.etDepartamento);
        campoFecha = (EditText) findViewById(R.id.etFecha);

        ConexionSQLiteHelper conn=new ConexionSQLiteHelper(this, "bd usuarios", null,1);
    }
    public  void  onClick(View view){
                registrarUsuario();

        }



    private void registrarUsuario() {
        ConexionSQLiteHelper conn=new ConexionSQLiteHelper(this, "bd usuarios", null,1);
        SQLiteDatabase db=conn.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(utilidades.CAMPO_CEDULA,campoCedula.getText().toString());
        values.put(utilidades.CAMPO_NOMBRE,campoNombre.getText().toString());
        values.put(utilidades.CAMPO_APELLIDO,campoApellido.getText().toString());
        values.put(utilidades.CAMPO_DEPARTAMENTO,campoDepartamento.getText().toString());
        values.put(utilidades.CAMPO_FECHA_,campoFecha.getText().toString());

        Long idResusltante=db.insert(utilidades.TABLA_USUARIO,utilidades.CAMPO_CEDULA,values);
        Toast.makeText(getApplicationContext(),"Registro insertado: "+idResusltante,Toast.LENGTH_SHORT).show();
}


}
