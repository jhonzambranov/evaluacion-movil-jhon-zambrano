package facci.jhonzambrano.administrativo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import facci.jhonzambrano.administrativo.unidades.utilidades;

public class Mostrar extends AppCompatActivity {
    EditText campoCedulaC, campoNombreC, campoApellidoC, campoDepartamentoC, campoFechaC;
    ConexionSQLiteHelper conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);

        conn=new ConexionSQLiteHelper(getApplicationContext(),"bd usuarios",null,1);

        campoCedulaC = (EditText) findViewById(R.id.etCedulaConsulta);
        campoNombreC = (EditText) findViewById(R.id.etNombreConsulta);
        campoApellidoC = (EditText) findViewById(R.id.etApellidoConsulta);
        campoDepartamentoC = (EditText) findViewById(R.id.etDepartamentoConsulta);
        campoFechaC = (EditText) findViewById(R.id.etFechaConsulta);
    }
    public  void  onClick(View view){
        ConsultarDatos();
    }

    private void ConsultarDatos() {
        SQLiteDatabase db=conn.getReadableDatabase();
        String[] parametros={campoCedulaC.getText().toString()};
        String[] campos={utilidades.CAMPO_NOMBRE,utilidades.CAMPO_APELLIDO,utilidades.CAMPO_DEPARTAMENTO,utilidades.CAMPO_FECHA_};

        try {
            Cursor cursor = db.query(utilidades.TABLA_USUARIO,campos,utilidades.CAMPO_CEDULA+"=?",parametros,null,null,null);
            cursor.moveToFirst();
            campoNombreC.setText((cursor.getString(0)));
            campoApellidoC.setText((cursor.getString(1)));
            campoDepartamentoC.setText((cursor.getString(2)));
            campoFechaC.setText((cursor.getString(3)));
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El registro no existe", Toast.LENGTH_LONG).show();
            Limpiar();
        }
    }

    private void Limpiar() {
        campoNombreC.setText("");
        campoApellidoC.setText("");
        campoDepartamentoC.setText("");
        campoFechaC.setText("");
    }
}
