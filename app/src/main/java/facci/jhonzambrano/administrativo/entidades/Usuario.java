package facci.jhonzambrano.administrativo.entidades;

public class Usuario {

    private String cedula;
    private String nombres;
    private String apellidos;
    private String depatamento;
    private String fecha;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDepatamento() {
        return depatamento;
    }

    public void setDepatamento(String depatamento) {
        this.depatamento = depatamento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Usuario(String cedula, String nombres, String apellidos, String depatamento, String fecha) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.depatamento = depatamento;
        this.fecha = fecha;


    }

}