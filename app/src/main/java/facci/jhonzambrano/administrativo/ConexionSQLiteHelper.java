package facci.jhonzambrano.administrativo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import facci.jhonzambrano.administrativo.unidades.utilidades;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {

    final  String CREAR_TABLA_USUARIO="CREATE TABLE usuarios (cedula TEXT, nombre TEXT, apellido TEXT, departamento TEXT, fecha TEXT)";
    public ConexionSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(utilidades.CREAR_TABLA_USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS usuarios");
        onCreate(db);
    }
}
